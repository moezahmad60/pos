<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{url('public/logo', $general_setting->site_logo)}}"/>
    <title>{{$general_setting->site_title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 14px;
            line-height: 24px;
            font-family: 'Ubuntu', sans-serif;
            text-transform: capitalize;
        }

        .table-order, .table-order th, .table-order td {
            border: 1px solid;
        }

        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor: pointer;
        }

        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }

        td,
        th,
        tr,
        table {
            border-collapse: collapse;
        }

        tr {
            border-bottom: 1px dotted #ddd;
        }

        td, th {
            padding: 7px 0;
            /*width: 50%;*/
        }

        table {
            width: 100%;
        }

        tfoot tr th:first-child {
            text-align: left;
        }

        .centered {
            text-align: center;
            align-content: center;
        }

        small {
            font-size: 11px;
        }

        @media print {
            * {
                font-size: 12px;
                line-height: 20px;
            }

            td, th {
                padding: 5px 0;
            }

            .hidden-print {
                display: none !important;
            }

            @page {
                margin: 0;
            }

            body {
                margin: 0.5cm;
                margin-bottom: 1.6cm;
            }
        }
    </style>
</head>
<body>

<div style="max-width:50%;margin:0 auto">
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../../pos'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> {{trans('file.Back')}}</a>
                </td>
                <td>
                    <button onclick="window.print();" class="btn btn-primary"><i
                            class="dripicons-print"></i> {{trans('file.Print')}}</button>
                </td>
            </tr>
        </table>
        <br>
    </div>

    <div id="receipt-data">

        <h1 style="text-align: center; font-size: 28px">{{$lims_warehouse_data->name}}</h1>
        <div style="font-weight: bold">
            <p>{{trans('file.Date')}}: {{$lims_sale_data->created_at}}</p>
            <p>
                {{trans('Invoice No')}}: {{$lims_warehouse_data->alias}} -BL- {{$lims_sale_data->id}}
            </p>
        </div>

        <br>

        <div class="centered">
            {{--            @if($general_setting->site_logo)--}}
            {{--                <img src="{{url('public/logo', $general_setting->site_logo)}}" height="42" width="42"--}}
            {{--                     style="margin:10px 0;filter: brightness(0);">--}}
            {{--            @endif--}}
            <p style="font-size: 18px">
                Shaukat Khanam Memorial Cancer Hospital
                Lahore,
            </p>
            <p style="font-size: 18px">
                (Purchase department)

            </p>


        </div>
        <div>
            <p style="font-size: 16px">
                Dear Sir,<br>

                We are submitting invoice against items delivered against following purchase order:

            </p>

        </div>
        {{--        <p>{{trans('file.Address')}}: {{$lims_warehouse_data->address}}--}}
        {{--            <br>{{trans('file.Phone Number')}}: {{$lims_warehouse_data->phone}}--}}
        {{--        </p>--}}
        {{--        <p>{{trans('file.Date')}}: {{$lims_sale_data->created_at}}<br>--}}
        {{--            {{trans('file.reference')}}: {{$lims_sale_data->reference_no}}<br>--}}
        {{--            {{trans('file.customer')}}: {{$lims_customer_data->name}}--}}
        {{--        </p>--}}
        <table class="table-order">
            <thead>
            <tr>
                <th>Order No.</th>
                <th>Invoice No.</th>
                <th>Delivery Challan</th>
                <th>Delivery Date</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$lims_sale_data->reference_no}}</td>
                <td>{{$lims_warehouse_data->alias}} -BL- {{$lims_sale_data->id}}</td>
                <td>{{$lims_delivery_data->challan ?? ''}}</td>
                <td>{{$lims_delivery_data->created_at ?? ''}}</td>
            </tr>
            </tbody>
        </table>
        <br> <br>
        <table class="table-order">
            <thead>
            <tr>
                <th>Sr #</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Price</th>
                <th>GST</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php $total_product_tax = 0; $ttlPrice = 0;?>
            <tr>
                @foreach($lims_product_sale_data as $product_sale_data)
                    <?php
                    $lims_product_data = \App\Product::find($product_sale_data->product_id);
                    if ($product_sale_data->variant_id) {
                        $variant_data = \App\Variant::find($product_sale_data->variant_id);
                        $product_name = $lims_product_data->name . ' [' . $variant_data->name . ']';
                    } else
                        $product_name = $lims_product_data->name;


                    ?>

                    <td>{{$product_sale_data->sale_id}}</td>
                    <td>{{$product_name}}</td>
                    <td>{{$product_sale_data->qty}}</td>
                    <td>{{$product_sale_data->net_unit_price}}</td>
                    <td> @if($product_sale_data->tax_rate)
                            <?php $total_product_tax += $product_sale_data->tax ?>
                            [({{$product_sale_data->tax_rate}}%): {{$product_sale_data->tax}}]
                        @endif</td>
                    <td>
                        {{number_format((float)$lims_sale_data->total_price, 2, '.', '')}}
                    </td>
                    {{--                    <td >{{number_format((float)$lims_sale_data->total_price, 2, '.', '')}}</td>--}}

                @endforeach
            </tr>
            <tr>
                <td></td>
                <td>Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    {{number_format((float)$lims_sale_data->total_price, 2, '.', '')}}
                </td>
            </tr>
            </tbody>
            <tfoot>
            {{--            <tr>--}}
            {{--                <th colspan="2">{{trans('file.Total')}}</th>--}}
            {{--                <th style="text-align:right">{{number_format((float)$lims_sale_data->total_price, 2, '.', '')}}</th>--}}
            {{--            </tr>--}}
            @if($general_setting->invoice_format == 'gst' && $general_setting->state == 1)
                <tr>
                    <td colspan="2">IGST</td>
                    <td style="text-align:right">{{number_format((float)$total_product_tax, 2, '.', '')}}</td>
                </tr>
            @elseif($general_setting->invoice_format == 'gst' && $general_setting->state == 2)
                <tr>
                    <td colspan="2">SGST</td>
                    <td style="text-align:right">{{number_format((float)($total_product_tax / 2), 2, '.', '')}}</td>
                </tr>
                <tr>
                    <td colspan="2">CGST</td>
                    <td style="text-align:right">{{number_format((float)($total_product_tax / 2), 2, '.', '')}}</td>
                </tr>
            @endif
            @if($lims_sale_data->order_tax)
                <tr>
                    <th colspan="2">{{trans('file.Order Tax')}}</th>
                    <th style="text-align:right">{{number_format((float)$lims_sale_data->order_tax, 2, '.', '')}}</th>
                </tr>
            @endif
            @if($lims_sale_data->order_discount)
                <tr>
                    <th colspan="2">{{trans('file.Order Discount')}}</th>
                    <th style="text-align:right">{{number_format((float)$lims_sale_data->order_discount, 2, '.', '')}}</th>
                </tr>
            @endif
            @if($lims_sale_data->coupon_discount)
                <tr>
                    <th colspan="2">{{trans('file.Coupon Discount')}}</th>
                    <th style="text-align:right">{{number_format((float)$lims_sale_data->coupon_discount, 2, '.', '')}}</th>
                </tr>
            @endif
            @if($lims_sale_data->shipping_cost)
                <tr>
                    <th colspan="2">{{trans('file.Shipping Cost')}}</th>
                    <th style="text-align:right">{{number_format((float)$lims_sale_data->shipping_cost, 2, '.', '')}}</th>
                </tr>
            @endif
            {{--            <tr>--}}
            {{--                <th colspan="2">{{trans('file.grand total')}}</th>--}}
            {{--                <th style="text-align:right">{{number_format((float)$lims_sale_data->grand_total, 2, '.', '')}}</th>--}}
            {{--            </tr>--}}
            {{--            <tr>--}}
            {{--                @if($general_setting->currency_position == 'prefix')--}}
            {{--                    <th class="centered" colspan="3">{{trans('file.In Words')}}: <span>{{$currency->code}}</span>--}}
            {{--                        <span>{{str_replace("-"," ",$numberInWords)}}</span></th>--}}
            {{--                @else--}}
            {{--                    <th class="centered" colspan="3">{{trans('file.In Words')}}:--}}
            {{--                        <span>{{str_replace("-"," ",$numberInWords)}}</span> <span>{{$currency->code}}</span></th>--}}
            {{--                @endif--}}
            {{--            </tr>--}}
            </tfoot>
        </table>
        <table style="display: none">
            <tbody>
            @foreach($lims_payment_data as $payment_data)
                <tr style="background-color:#ddd;">
                    <td style="padding: 5px;width:30%">{{trans('file.Paid By')}}: {{$payment_data->paying_method}}</td>
                    <td style="padding: 5px;width:40%">{{trans('file.Amount')}}
                        : {{number_format((float)$payment_data->amount, 2, '.', '')}}</td>
                    <td style="padding: 5px;width:30%">{{trans('file.Change')}}
                        : {{number_format((float)$payment_data->change, 2, '.', '')}}</td>
                </tr>
            @endforeach
            <tr>
                <td class="centered"
                    colspan="3">{{trans('file.Thank you for shopping with us. Please come again')}}</td>
            </tr>
            <tr>
                <td class="centered" colspan="3">
                    <?php echo '<img style="margin-top:10px;" src="data:image/png;base64,' . DNS1D::getBarcodePNG($lims_sale_data->reference_no, 'C128') . '" width="300" alt="barcode"   />';?>
                    <br>
                    <?php echo '<img style="margin-top:10px;" src="data:image/png;base64,' . DNS2D::getBarcodePNG($lims_sale_data->reference_no, 'QRCODE') . '" alt="barcode"   />';?>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="centered" style="margin:30px 0 50px">
            <p><strong> OUR TERMS AND CONDITIONS ARE AS UNDER:</strong>
            </p>

        </div>
        <ol>
            <li>Rate is free delivery at your hospital premises
            </li>
            <li>Delivery period 15 days or earlier after the date of receipt of order.
            </li>
        </ol>

        <p>Thanking you.</p>
        <p>Yours truly</p>
        <p>Umer Anwar Bhalli</p>
        <p><strong>{{$lims_warehouse_data->name}}</strong></p>
        <p><strong><i>NTN: 4192319-7
                </i></strong></p>
    </div>
</div>

<script type="text/javascript">
    function auto_print() {
        window.print()
    }

    setTimeout(auto_print, 1000);
</script>

</body>
</html>
